package org.blog.test.service.impl;

import lombok.extern.slf4j.Slf4j;
import org.blog.test.service.AsyncService;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Service;

@Async
@Slf4j
@Service
public class AsyncServiceImpl implements AsyncService {

    @Override
    public void doAsync() {
        log.info("async method");
    }
}
