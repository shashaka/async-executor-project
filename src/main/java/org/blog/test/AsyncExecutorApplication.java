package org.blog.test;

import org.blog.test.service.AsyncService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class AsyncExecutorApplication implements CommandLineRunner {
    public static void main(String[] args) {
        SpringApplication.run(AsyncExecutorApplication.class, args);
    }

    @Autowired
    private AsyncService asyncService;

    @Override
    public void run(String... args) throws Exception {
        asyncService.doAsync();
    }
}